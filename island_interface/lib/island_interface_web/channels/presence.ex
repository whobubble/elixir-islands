defmodule IslandInterfaceWeb.Presence do
  use Phoenix.Presence,
    otp_app: :island_interface,
    pubsub_server: IslandInterface.PubSub
end
