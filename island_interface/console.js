function position_island(channel, player, island, row, col) {
    var params = { "player": player, "island": island, "row": row, "col": col }
    channel.push("position_island", params)
        .receive("ok", response => {
            console.log("Island positioned!", response)
        })
        .receive("error", response => {
            console.log("Unable to position island.", response)
        })
}

function set_islands(channel, player) {
    channel.push("set_islands", player)
        .receive("ok", response => {
            console.log("Here is the board:");
            console.dir(response.board)
        })
        .receive("error", response => {
            console.log("Unable to set islands for: " + player, response)
        })
}

game_channel.on("player_set_islands", response => {
    console.log("Player Set Islands", response)
})

function guess_coordinate(channel, player, row, col) {
    var params = { "player": player, "row": row, "col": col }
    channel.push("guess_coordinate", params)
        .receive("error", response => {
            console.log("Unable to guess a coordinate: " + player, response)
        })
}

game_channel.on("player_guessed_coordinate", response => {
    console.log("Player Guessed Coordinate: ", response.result)
})

// load phoenix and join
var phoenix = require("phoenix")

var socket = new phoenix.Socket("/socket", {})

socket.connect()

function new_channel(player, screen_name) {
    return socket.channel("game:" + player, { screen_name: screen_name });
}

function join(channel) {
    channel.join()
        .receive("ok", response => {
            console.log("Joined successfully!", response);
        })
        .receive("error", response => { console.log("Unable to join", response) })
}

var game_channel = new_channel("moon", "moon")

game_channel.on("subscribers", response => {
    console.log("These players have joined: ", response)
})

var game_channel = new_channel("moon", "diva")

game_channel.on("subscribers", response => {
    console.log("These players have joined: ", response)
})

join(game_channel)

game_channel.push("show_subscribers")